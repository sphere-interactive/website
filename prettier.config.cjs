/** @type {import('prettier').Config} */
const config = {
    trailingComma: 'all',
    arrowParens: 'avoid',
    semi: false,
    singleQuote: true,
    printWidth: 100,

    // Plugins
    plugins: [require('prettier-plugin-astro'), require('prettier-plugin-tailwindcss')],

    // Overrides and options
    overrides: [
        { files: '*.astro', options: { parser: 'astro' } },
        { files: '*.vue', options: { vueIndentScriptAndStyle: true } },
    ],
    tailwindConfig: './tailwind.config.cjs',
}

module.exports = config
