# **Sphere Interactive**

Official Website. Made with Astro, Vue and TailwindCSS.
[Visit Here](https://sphere-interactive.com)

To run a development server, clone the repo and run:

```
npm run dev
```

To build for production, clone the repo and run:

```
npm run build
```
