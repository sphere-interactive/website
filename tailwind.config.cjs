const defaultTheme = require('tailwindcss/defaultTheme')

/** @type {import('tailwindcss').Config} */
module.exports = {
    content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
    theme: {
        extend: {
            fontFamily: {
                opensans: ['Open Sans', ...defaultTheme.fontFamily.sans],
                poppins: ['Poppins', ...defaultTheme.fontFamily.sans],
            },
            colors: {
                t1: '#8ba5d2',
                t2: '#284069',
                t3: '#252e54',
                t4: '#0c131f',
                t5: '#e5e5e5',
                t6: '#97aed7',
            },
            backgroundImage: {
                'hero-1': "url('/img/backgrounds/1.avif')",
                'hero-2': "url('/img/backgrounds/2.avif')",
                'hero-3': "url('/img/backgrounds/3.avif')",
                'hero-4': "url('/img/backgrounds/4.avif')",
                'hero-5': "url('/img/backgrounds/5.avif')",
                'hero-6': "url('/img/backgrounds/6.avif')",
                'hero-7': "url('/img/backgrounds/7.avif')",
                'hero-8': "url('/img/backgrounds/8.avif')",
                'hero-9': "url('/img/backgrounds/9.avif')",
                'hero-10': "url('/img/backgrounds/10.avif')",
                'hero-11': "url('/img/backgrounds/11.avif')",
                'hero-12': "url('/img/backgrounds/12.avif')",
            },
        },
    },
    plugins: [require('@tailwindcss/typography')],
}
